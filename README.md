# Spotippos App

## Usage

First we need npm and node >= 6.2.0 then install the deps with

`npm install`

## Developing

This app uses webpack-dev-server to controll the assets so just run `npm start` and check the http://localhost:8080

## How to build

`npm run build`

it will run the lint and the tests to check if everything is ok.  
if everything went ok it generates a dist folder ready to be deployed

## Production mode

There's a docker-compose file to test the build process and if it's working with nginx, just run

`docker-compose up`

Now you can access http://localhost:8080 and see how it will work in prod mode
