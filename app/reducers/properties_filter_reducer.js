import _ from 'lodash';
import filterProperty from '../helpers/filter_property';

const defaultState = {
  data: [],
};

export default function propertiesFilterReducer({ type, payload }, stateObservable) {
  switch (type) {
    case 'FILTER_PROPERTIES': {
      return stateObservable.map((state = defaultState) => {
        const filter = _.partial(filterProperty, payload);
        const properties = _.filter(state.data.items, filter);
        return _.assign({}, state, { properties });
      });
    }

    default:
      return stateObservable;
  }
}
