/* eslint no-param-reassign: ["error", { "props": false }] */
import React, { Component } from 'react';
import SpotButton from '../button';

// eslint-disable-next-line
import style from './filter.styl';

class SpotFilter extends Component {
  constructor(props) {
    super(props);
    this.store = props.store;
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(evt) {
    evt.preventDefault();
    this.store.dispatch({
      type: 'FILTER_PROPERTIES',
      payload: this.serializeForm(),
    });
  }

  serializeForm() {
    return Object.keys(this.refs).reduce((memo, key) => {
      const input = this.refs[key];
      const value = input.value;

      if (value !== '') {
        memo[key] = parseInt(value, 10);
      }

      return memo;
    }, {});
  }

  render() {
    return (
      <form className="spot-filter" onSubmit={this.onSubmit}>
        <h3 className="spot-filter__title">Filtro</h3>
        <div className="spot-filter__field_group">
          <div className="spot-filter__field">
            <label htmlFor="id">ID</label>
            <input min="0" ref="id" name="id" type="number" />
          </div>
          <div className="spot-filter__field">
            <label htmlFor="squareMeters">Area</label>
            <input min="0" ref="squareMeters" name="squareMeters" type="number" />
          </div>
        </div>

        <div className="spot-filter__field_group">
          <div className="spot-filter__field">
            <label htmlFor="beds">Quartos</label>
            <input min="0" ref="beds" name="beds" type="number" />
          </div>
          <div className="spot-filter__field">
            <label htmlFor="baths">Banheiros</label>
            <input min="0" ref="baths" name="baths" type="number" />
          </div>
        </div>

        <div className="spot-filter__field_group">
          <label htmlFor="minPrice">Valor</label>
          <div className="spot-filter__field">
            <input min="0" ref="minPrice" placeholder="Mínimo" name="minPrice" type="number" />
          </div>

          <div className="spot-filter__field">
            <input min="0" ref="maxPrice" placeholder="Máximo" name="maxPrice" type="number" />
          </div>
        </div>

        <SpotButton label="Filtrar" type="submit">Filtrar</SpotButton>
      </form>
    );
  }
}

SpotFilter.propTypes = {
  store: React.PropTypes.object.isRequired,
};

export default SpotFilter;
