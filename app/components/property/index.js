import React from 'react';
// eslint-disable-next-line
import style from './property.styl';

import SpotCard from '../card';
import SpotButton from '../button';
import SpotThumb from '../thumb';
import SpotIcon from '../icon';
import formatPrice from '../../helpers/format_price';
import defaultThumb from './assets/placeholder.png';

const spotPropertyFooter = ({ beds, baths, squareMeters }) => (
  <div className="row">
    <div className="row col-xs-12 col-sm-8 start-sm">
      <div className="col-sm-3 spot-property__footer__text">
        <SpotIcon icon="area" />
        <span className="spot-property__footer__text">{squareMeters} M²</span>
      </div>
      <div className="col-sm-4 spot-property__footer__text">
        <SpotIcon icon="bed" />
        <span >{beds} Quartos</span>
      </div>
      <div className="col-sm-4 spot-property__footer__text">
        <SpotIcon icon="bath" />
        <span>{baths} Banheiros</span>
      </div>
    </div>
    <div className="row center-xs end-sm col-xs-12 col-sm-4">
      <SpotButton label="Visualizar Anúncio" className="col-xs">Visualizar Anúncio</SpotButton>
    </div>
  </div>
);

spotPropertyFooter.propTypes = {
  beds: React.PropTypes.string.isRequired,
  baths: React.PropTypes.string.isRequired,
  squareMeters: React.PropTypes.string.isRequired,
};

const SpotProperty = ({ property }) => {
  const { thumb, title, id, description, price } = property;

  return (
    <SpotCard
      className="spot-property"
      thumb={
        <div className="spot-property__thumb">
          <SpotThumb src={thumb || defaultThumb} alt={title} title={title} />
          <span className="spot-property__price">R$ {formatPrice(price)}</span>
        </div>
      }
      footer={spotPropertyFooter(property)}
    >
      <span className="spot-property__id">ID: {id}</span>
      <h3 className="spot-property__title">{title}</h3>
      <p className="spot-property__text">{description}</p>
    </SpotCard>
  );
};

SpotProperty.propTypes = {
  property: React.PropTypes.shape({
    thumb: React.PropTypes.string.isRequired,
    price: React.PropTypes.string.isRequired,
    title: React.PropTypes.string.isRequired,
    id: React.PropTypes.string.isRequired,
    description: React.PropTypes.string.isRequired,
    beds: React.PropTypes.string.isRequired,
    baths: React.PropTypes.string.isRequired,
    squareMeters: React.PropTypes.string.isRequired,
  }).isRequired,
};

export default SpotProperty;
