import React, { Component } from 'react';
import Rx from 'rx';
import SpotMenu from '../menu';
// eslint-disable-next-line
import style from './sidenav.styl';

class SpotSidenav extends Component {
  constructor(props) {
    super(props);
    this.state = { isOpen: props.isOpen };
    this.toggle = this.toggle.bind(this);
    this.closeByKey = this.closeByKey.bind(this);
  }

  componentDidMount() {
    this.subscription = Rx.Observable
      .fromEvent(document.body, 'keydown')
      .subscribe((evt) => this.closeByKey(evt));
  }

  componentWillUnmount() {
    this.subscription.dispose();
  }

  toggle() {
    this.setState({ isOpen: !this.state.isOpen });
  }

  closeByKey(evt) {
    if (this.state.isOpen && evt.key === 'Escape') {
      this.setState({ isOpen: false });
    }
  }


  render() {
    const { className } = this.props;
    const isOpen = this.state.isOpen ? 'spot-sidebar--is-open' : '';

    return (
      <div className={`${className} spot-sidebar  ${isOpen}`}>
        <div className="spot-sidebar__backdrop" onClick={this.toggle}></div>
        <nav className="spot-sidebar__nav" role="navigation">
          <header className="spot-sidebar__title">
            <h2>Spotippos</h2>
          </header>
          <SpotMenu onClick={this.toggle} />
        </nav>
      </div>
    );
  }
}

SpotSidenav.propTypes = {
  className: React.PropTypes.string,
  isOpen: React.PropTypes.bool,
};

export default SpotSidenav;
