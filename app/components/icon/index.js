import React from 'react';

// eslint-disable-next-line
import style from './icon.styl';

const SpotIcon = ({ icon }) => (
  <i aria-label="hidden" className={`spot-icon spot-icon__${icon}`}></i>
);

SpotIcon.propTypes = {
  icon: React.PropTypes.string.isRequired,
};

export default SpotIcon;
