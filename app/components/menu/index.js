import React, { Component } from 'react';
import SpotIcon from '../icon';

// eslint-disable-next-line
import style from './menu.styl';

const SpotMenuItem = ({ icon, text, onClick }) => (
  <a href="/" onClick={onClick}>
    <SpotIcon icon={icon} />
    <span className="spot-menu__item-label">
      {text}
    </span>
  </a>
);

SpotMenuItem.propTypes = {
  icon: React.PropTypes.string.isRequired,
  text: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func.isRequired,
};

class SpotMenu extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(evt) {
    if (this.props.onClick) {
      evt.preventDefault();
      this.props.onClick(evt);
    }
  }

  render() {
    return (
      <ul className="spot-menu">
        <li className="spot-menu__item spot-menu__item--is-active">
          <SpotMenuItem onClick={this.onClick} icon="building" text="Anúncios" />
        </li>
        <li className="spot-menu__item">
          <SpotMenuItem onClick={this.onClick} icon="add" text="Novo Anúncio" />
        </li>
      </ul>
    );
  }
}

SpotMenu.propTypes = {
  onClick: React.PropTypes.func,
};

export default SpotMenu;
