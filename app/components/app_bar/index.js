import React from 'react';
import SpotIcon from '../icon';

// eslint-disable-next-line
import style from './app_bar.styl';

const SpotAppBar = ({ onClick }) => (
  <header className="spot-app-bar" role="banner">
    <div className="spot-app-bar__icon" onClick={onClick}>
      <SpotIcon icon="menu" />
    </div>
    <div className="spot-app-bar__text">
      <h1 className="spot-app-bar__title">Spotippos Anúncios</h1>
    </div>
  </header>
);

SpotAppBar.propTypes = {
  onClick: React.PropTypes.func.isRequired,
};

export default SpotAppBar;
