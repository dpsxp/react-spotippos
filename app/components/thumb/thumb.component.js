import React from 'react';

const thumb = ({ src, alt, title, className }) => (
  <img className={className} src={src} alt={alt} title={title} />
);

thumb.propTypes = {
  src: React.PropTypes.string.isRequired,
  alt: React.PropTypes.string.isRequired,
  title: React.PropTypes.string.isRequired,
  className: React.PropTypes.string,
};

export default thumb;
