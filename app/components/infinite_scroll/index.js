import React, { Component } from 'react';
import Rx from 'rx';

// eslint-disable-next-line
import style from './infinite_scroll.styl';

class SpotInfiniteScroll extends Component {
  componentDidMount() {
    const self = this;
    this.subscription = Rx.Observable
      .fromEvent(window, 'scroll')
      .debounce(500)
      .subscribe(() => {
        const height = window.screen.height;
        const proximity = height / 100;
        const top = self.refs.inifiteScroll.getBoundingClientRect().top + proximity;

        if (top <= height) {
          self.props.onVisible();
        }
      });
  }

  componentWillUnmount() {
    this.subscription.dispose();
  }

  render() {
    return <div className="spot-infinite-scroll" ref="inifiteScroll"></div>;
  }
}

SpotInfiniteScroll.propTypes = {
  onVisible: React.PropTypes.func.isRequired,
};

export default SpotInfiniteScroll;
