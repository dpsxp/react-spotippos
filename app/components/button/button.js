import React from 'react';

const SpotButton = ({ children, color, type, onClick, label }) => (
  <button
    onClick={onClick}
    type={type || 'button'}
    role="button"
    aria-label={label}
    className={`spot-button__${color || 'primary'} spot-button`}
  >
    {children}
  </button>
);

SpotButton.propTypes = {
  children: React.PropTypes.node.isRequired,
  color: React.PropTypes.string,
  type: React.PropTypes.string,
  label: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func,
};

export default SpotButton;
