import React from 'react';

// eslint-disable-next-line
import styles from './loader.styl';

const SpotLoader = () => (
  <div className="spot-loader"></div>
);

export default SpotLoader;
