import _ from 'lodash';

const formatPrice = _.memoize((value) => (
  value.replace(/(\d{1,3})/g, '$1.').replace(/(\.0{1,2}\.$)|(\.$)/, ',00')
));

export default formatPrice;
