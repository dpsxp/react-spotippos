import _ from 'lodash';

function filterPrice(params, property) {
  const { maxPrice, minPrice } = params;
  const { price } = property;

  if (maxPrice && minPrice) {
    return price <= maxPrice && price >= minPrice;
  } else if (maxPrice && !minPrice) {
    return price <= maxPrice;
  } else if (!maxPrice && minPrice) {
    return price >= minPrice;
  }

  return false;
}

function isEqual(value, otherValue) {
  return value === parseInt(otherValue, 10);
}

export default function filterProperty(params, property) {
  const { maxPrice, minPrice } = params;
  const comparableValues = ['id', 'beds', 'baths', 'squareMeters'];

  if (_.keys(params).length === 0) {
    return true;
  }

  let valid = true;

  if (maxPrice || minPrice) {
    valid = filterPrice(params, property);
  }

  valid = comparableValues
    .filter(key => params[key])
    .reduce((result, key) => result && isEqual(params[key], property[key]), valid);


  return valid;
}
