FROM node:6.2.0

WORKDIR /app

ADD package.json .

RUN npm install

ADD . .

EXPOSE 8080

ONBUILD RUN npm run build

VOLUME ["/app"]

CMD ["npm", "start"]
