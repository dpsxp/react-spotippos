const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const config = require('./webpack.config');
const gzip = new CompressionPlugin({
  asset: '[path].gz',
  test: /\.js$|\.html$|\.css$/,
});

const define = new webpack.DefinePlugin({
  'process.env': {
    NODE_ENV: JSON.stringify('production'),
  },
});

config.plugins.push(gzip);
config.plugins.push(define);

module.exports = Object.assign({}, config, {
  devtool: '',
  devServer: {},
});
