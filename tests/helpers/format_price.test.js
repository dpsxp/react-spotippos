import formatPrice from 'app/helpers/format_price';
import { expect } from 'chai';

describe('#formatPrice', () => {
  it('parses the given value to a currency format', () => {
    const value = '1800000';
    expect(formatPrice(value)).to.be.equal('180.000,00');
  });

  it('sets the last two numbers as the cents value', () => {
    const value = '18000000';
    expect(formatPrice(value)).to.be.equal('180.000,00');
  });
});
