import React from 'react';
import storeFactory from 'app/store';
import { mount } from 'enzyme';
import { expect } from 'chai';
import SpotFilter from 'app/components/filter'

describe('Components', () => {
  const appStore = storeFactory();

  describe('<SpotFiter />', () => {
    let component;

    beforeEach(() => {
      component = mount(<SpotFilter store={appStore} />);
    });

    describe('#serializeForm', () => {
      it('serializes the form with correct values', () => {
        component.node.refs.id.value = '10';
        component.node.refs.beds.value = '5';
        component.node.refs.baths.value = '2';
        component.node.refs.squareMeters.value = '10';
        component.node.refs.minPrice.value = '50';
        component.node.refs.maxPrice.value = '100';

        const result = component.node.serializeForm();

        expect(result).to.be.eql({
          id: 10,
          beds: 5,
          baths: 2,
          squareMeters: 10,
          minPrice: 50,
          maxPrice: 100,
        });
      });

      it('returns only keys that have any value', () => {
        component.node.refs.id.value = '10';
        component.node.refs.beds.value = '5';
        component.node.refs.baths.value = '2';

        expect(component.node.serializeForm()).to.be.eql({ id: 10, beds: 5, baths: 2 });
      });

      it('returns a empty object when all inputs has no values', () => {
        expect(component.node.serializeForm()).to.be.eql({});
      });
    });

    describe('#onSubmit', () => {
      it('dispatchs a FILTER_PROPERTIES action whit the from data serialized', () => {
        sinon.spy(appStore, 'dispatch');
        const fakeData = { id: 32 };
        sinon.stub(component.node, 'serializeForm', () => fakeData);

        component.find('form').simulate('submit');
        const action = { type: 'FILTER_PROPERTIES', payload: fakeData };

        expect(appStore.dispatch.calledWith(action)).to.be.eql(true);
      });
    });
  });
});
