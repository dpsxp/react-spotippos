import React from 'react';
import storeFactory from 'app/store';
import { mount } from 'enzyme';
import { expect } from 'chai';
import _ from 'lodash';
import SpotPropertiesList from 'app/components/properties_list';
import SpotProperty from 'app/components/property';
import data from '../fixtures/properties.json';

describe('Components', () => {
  let appStore;

  describe('<SpotPropertiesList />', () => {
    beforeEach(() => {
      appStore = storeFactory();
      appStore.apply((action, state) => (
        state.map((newState) => _.assign({}, newState, { properties: data }))
      ));
    });

    it('render SpotProperty component for each property on the current state', () => {
      sinon.stub(appStore, 'getState', () => ({ properties: data }));
      const component = mount(<SpotPropertiesList store={appStore} />);
      expect(component.find(SpotProperty).length).to.be.eql(data.length);
    });

    it('updates the properties state when the store changes', (done) => {
      const component = mount(<SpotPropertiesList store={appStore} />);

      appStore.subscribe(({ properties }) => {
        expect(component.state('properties')).to.be.eql(properties);
        done();
      });

      appStore.dispatch({ type: 'MY_TEST' });
    });
  });
});
