import React from 'react';
import storeFactory from 'app/store';
import { mount } from 'enzyme';
import { expect } from 'chai';
import App from 'app/app';
import SpotSidenav from 'app/components/sidenav';

describe('Components', () => {
  describe('<App />', () => {
    let wrapper = {};

    beforeEach(() => {
      const state = { data: { page: 1 }, properties: [] };
      const appStore = storeFactory();
      sinon.stub(appStore, 'getState', () => state);
      wrapper = mount(<App store={appStore} />);
    });

    afterEach(() => {
      wrapper.unmount();
    });

    describe('#toggleSideBar', () => {
      it('toggles the SpotSidenav component', () => {
        const spy = sinon.spy(wrapper.node.refs.sideNav, 'toggle');
        wrapper.node.toggleSideBar();
        expect(wrapper.node.refs.sideNav).to.be.instanceof(SpotSidenav);
        expect(spy.calledOnce).to.be.eql(true);
      });
    });

    describe('#componentWillUnmount', () => {
      it('unsubscribe on the current store to avoid memory leaks and ghost events', () => {
        const spy = sinon.spy(wrapper.node.subscription, 'dispose');
        wrapper.unmount();
        expect(spy.calledOnce).to.be.eql(true);
      });
    });

    describe('#loadMore', () => {
      it('updates the page state and calls the loadProperties', () => {
        const prevPage = wrapper.state('page');
        const spy = sinon.spy();
        sinon.stub(wrapper.node, 'loadProperties', spy);

        wrapper.node.loadMore();

        expect(wrapper.state('page')).to.be.eql(prevPage + 1);
        expect(wrapper.state('loading')).to.be.eql(true);
        expect(spy.calledOnce).to.be.eql(true);
      });
    });

    describe('#loadProperties', () => {
      it('dispatch LOAD_PROPERTIES action', () => {
        const spy = sinon.spy(wrapper.node.store, 'dispatch');
        const action = {
          type: 'LOAD_PROPERTIES',
          payload: { page: wrapper.state('page') },
        };

        wrapper.node.loadProperties();

        expect(spy.calledWith(action)).to.be.eql(true);
      });
    });
  });
});
