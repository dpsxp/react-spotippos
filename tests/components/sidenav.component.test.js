import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import SpotSidenav from 'app/components/sidenav';
import simulant from 'simulant';


describe('Components', () => {
  describe('<SpotSidenav />', () => {
    let wrapper;
    const keydownEvent = simulant('keydown', { key: 'Escape', keyCode: '27' });

    beforeEach(() => {
      wrapper = mount(<SpotSidenav />);
    });

    describe('#toggle', () => {
      it('toggles the isOpen state property', () => {
        const prev = wrapper.state('isOpen');

        wrapper.node.toggle();

        expect(wrapper.state('isOpen')).to.be.eql(!prev);
      });
    });

    describe('#closeByEsc', () => {
      it('closes the sidebar when the ESC key is pressed', () => {
        wrapper.setState({ isOpen: true });

        document.body.dispatchEvent(keydownEvent);

        expect(wrapper.state('isOpen')).to.be.eql(false);
      });
    });

    describe('#componentWillUnmount', () => {
      it('stops listening for keydown event', () => {
        sinon.spy(wrapper.node, 'closeByKey');

        wrapper.unmount();
        document.body.dispatchEvent(keydownEvent);

        expect(wrapper.node.closeByKey.callCount).to.be.eql(0);
      });
    });

  });
});
